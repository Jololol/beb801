#include <ros/ros.h>
#include "opencv2/cudaarithm.hpp"
#include <image_transport/image_transport.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

#include "std_msgs/Float32.h"
#include <iostream>
#include <math.h>


enum class HMMPatch{
	Center,
	Bottom,
	Left,
	Right,
};


// Function to return the global thread index.
// Used for accessing elements of the GpuMat
__device__ int getGlobalIdx_2D_2D();

// Function to find the HMM Transition probability for each pixel location
__global__ void HMMTransition(cv::cuda::PtrStepSzf input, cv::cuda::PtrStepSzf output, 
	                            cv::cuda::PtrStepSzf patch, int patch_size);

// Performs an element-by-element multiplication of left and right, with the end result
// being stored in left. The equivalent of calling left *= right;
__global__ void multiplyGpuMat(cv::cuda::PtrStepSzf left, cv::cuda::PtrStepSzf right);


class HMMFilter {
private:
	ros::NodeHandle nh;
	image_transport::ImageTransport it_;
	image_transport::Subscriber image_sub;
	ros::Publisher hmm_pub;
	std_msgs::Float32 msg;

	
	float yk_prev = 0; // Remembers previous gamma value
	bool init = false; // Used to see if filter has been intialised
	int patch_size;
	HMMPatch patch_type; // Enum to store type of patch bening used
 
	cv::cuda::GpuMat alpha_prev;
	cv::cuda::GpuMat alpha;
	cv::cuda::GpuMat patch;

	cv::VideoWriter video;

public:
	HMMFilter(HMMPatch patchType, int patchSize) : it_(nh)
	{
		image_sub = it_.subscribe("image_morph/bottom_hat", 100, &HMMFilter::HMM, this);
		hmm_pub = nh.advertise<std_msgs::Float32>("temporal/HMM", 1000);    					 // Publish Viterbi output
		std::cout << "HMMFilter constructor called..." << std::endl;
		// Initialise member variables
		patch_size = patchSize;
		patch_type = patchType;
		// Initialise the patch
		patchInit();
			
		
	}

	~HMMFilter()
	{

	}


	void HMM(const sensor_msgs::ImageConstPtr &msg_in)
	{
		
		cv_bridge::CvImagePtr cv_ptr; // CvImagePtr used to reference cv Mat

		try {
			cv_ptr = cv_bridge::toCvCopy(msg_in, sensor_msgs::image_encodings::TYPE_32FC1);
		} catch (cv_bridge::Exception& e) {
			ROS_ERROR("cv_bridge exception: %s", e.what());
		}

		// If this is the first frame, initialise the alpha_prev mat
		if (!init) {
			std::cout << "Initialising HMM filter with first image frame..." << std::endl;
			initAlpha(cv_ptr->image.cols, cv_ptr->image.rows);
			init = true;
		}

		// Perform HMM filtering on frame
		cv::Mat output = processFrame(cv_ptr->image);

	
		// Publish the output data
		msg.data = yk_prev;
		hmm_pub.publish(msg);

	}

	cv::Mat processFrame(cv::Mat input)
	{
		float Beta = 0.9;
		
	
		// Variables for calling CUDA Kernel
		dim3 threadsPerBlock(8, 8);
		dim3 numBlocks(
			static_cast<int>(std::ceil(input.size().width /
																 static_cast<double>(threadsPerBlock.x))),
			static_cast<int>(std::ceil(input.size().height /
																 static_cast<double>(threadsPerBlock.y))));
			
		// Upload input into GpuMat alpha
		alpha.upload(input);
		// Add +1 to output from Morphology
		alpha.convertTo(alpha, alpha.type(), 1.0, 1.0);

		// Temporary storage for output from HMMTransition
		cv::cuda::GpuMat dst(input.rows, input.cols, input.type()); 
	
		// Finding HmmTransition for specified patch and size
		HMMTransition <<< numBlocks, threadsPerBlock>>>(alpha_prev, dst, patch, patch_size);
		cudaDeviceSynchronize(); // Wait for device to complete previous kernel call
		
		// Multiply results from HMMTransition
		multiplyGpuMat <<< numBlocks, threadsPerBlock>>>(alpha, dst);
		cudaDeviceSynchronize();
				
		// Get normalization factor by summing all elements of alpha
		// and inverting
		float N = 1.0/(cv::cuda::sum(alpha)[0]);
		// Multiply N by normalization factor 
		alpha.convertTo(alpha, alpha.type(), N);
				
		// Update alpha_prev
		alpha_prev = alpha.clone();
		
		// Download alpha result to standard CV::Mat
		cv::Mat output;
		alpha.download(output);

		float yk = (Beta * yk_prev) + (1 - Beta)*log(1.0/N);

		//std::cout << "Test statistic yk = " << yk <<  std::endl;

		yk_prev = yk;
	
		return output;
	}

private:
	void patchInit(){

		// probFactor varies depending on size of frame
		float probFactor;

		// Create the probablity patch for Center Patch
		// Ex: [1/3 1/3 1/3]
		//		 [1/3 1/3 1/3]
		//		 [1/3 1/3 1/3]
		if(patch_type == HMMPatch::Center){
			probFactor = 1.0 / (patch_size * patch_size);
			cv::Mat alpha_init(patch_size, patch_size, CV_32FC1, cv::Scalar::all(probFactor));
			std::cout << "Patch: " << alpha_init;
			patch.upload(alpha_init);
			return;
		}

		// Create the probablity patch for Bottom  Patch 
		// Ex: [ 0   0   0 ]
		//		 [1/3 1/3 1/3]
		//		 [1/3 1/3 1/3]
		if(patch_type == HMMPatch::Bottom){
			int subPatchx = patch_size;
			int subPatchy = patch_size/2 + 1;
			int offset = subPatchy - 1;
			probFactor = 1.0/(subPatchx * subPatchy);
			// Pre-fill with zeros
			cv::Mat alpha_init(patch_size, patch_size, CV_32FC1, cv::Scalar::all(0));
			for(int i = 0; i < subPatchy; i++){
				for(int j = 0; j < subPatchx; j++){
					alpha_init.at<float>(i+offset,j) = probFactor;
				}
			}
			std::cout << "Patch: " << alpha_init;
			patch.upload(alpha_init);
			return;
		}

		// Create the probablity patch for Bottom-Left Patch 
		// Ex: [ 0   0   0 ]
		//		 [1/3 1/3  0 ]
		//		 [1/3 1/3  0 ]
		if(patch_type == HMMPatch::Left){
			int subPatch = patch_size/2 + 1;
			int offset = subPatch - 1;
			std::cout << "Temp = :" << subPatch << std::endl;
			probFactor = 1.0/(subPatch*subPatch);
			// Pre-fill with zeros
			cv::Mat alpha_init(patch_size, patch_size, CV_32FC1, cv::Scalar::all(0));
			for(int i = 0; i < subPatch; i++){
				for(int j = 0; j < subPatch; j++){
					alpha_init.at<float>(i+offset,j+offset) = probFactor;
				}
			}
			std::cout << "Patch: " << alpha_init;
			patch.upload(alpha_init);
			return;
		}

		// Create the probablity patch for Bottom-Right Patch 
		// Ex: [ 0   0   0 ]
		//		 [ 0  1/3 1/3]
		//		 [ 0  1/3 1/3]
		if(patch_type == HMMPatch::Right){
			int subPatch = patch_size/2 + 1;
			int offset = subPatch - 1;
			std::cout << "Temp = :" << subPatch << std::endl;
			probFactor = 1.0/(subPatch*subPatch);
			// Pre-fill with zeros
			cv::Mat alpha_init(patch_size, patch_size, CV_32FC1, cv::Scalar::all(0));
			for(int i = 0; i < subPatch; i++){
				for(int j = 0; j < subPatch; j++){
					alpha_init.at<float>(i+offset,j+offset) = probFactor;
				}
			}
			std::cout << "Patch: " << alpha_init;
			patch.upload(alpha_init);
			return;
		}


	}

	void initAlpha(int width, int height)
	{
		float N = 1.0/(width*height);

		cv::Mat alpha_init(width, height, CV_32FC1, cv::Scalar::all(N));
	
		alpha_prev.upload(alpha_init);

	}


};

int main(int argc, char** argv)
{
	if(argc < 3){
		std::cout << "A patch type and size needs to be specified using syntax: ";
		std::cout << "HMM <Patch Type> <Patch Size>." << std::endl;
		std::cout << "Patch size must be an odd number. Even numbers will be rounded up." << std::endl;
		std::cout << "The patch type options are: " << std::endl;
		std::cout << "1: Center" << std::endl;
		std::cout << "2: Bottom" << std::endl;
		std::cout << "3: Left" << std::endl;
		std::cout << "4: Right" << std::endl; 
		return 0;
	}
	
	int patch_size = atoi(argv[2]);
	if(patch_size % 2 == 0){
		std::cout << "Patch size " << patch_size << " is even. Rounding up to " << patch_size + 1 << std::endl;
		patch_size++;
	}
	int patchNo = atoi(argv[1]);
	if(patchNo < 1 || patchNo > 4){
		std::cout << "ERROR: Patch type needs to be a number between 1-4." << std::endl;
		return 0;
	}

	HMMPatch patchType;
	switch(patchNo){
		case 1:
			patchType = HMMPatch::Center;
			break;
		case 2:
			patchType = HMMPatch::Bottom;
			break;
		case 3:
			patchType = HMMPatch::Left;
			break;
		case 4:
			patchType = HMMPatch::Right;
			break;
	}

	ros::init(argc, argv, "hmm");
	// Start the HMM Filter
	HMMFilter hmmFilter(patchType, patch_size);
	ros::spin();
	return 0;
}

__global__ void multiplyGpuMat(cv::cuda::PtrStepSzf left, cv::cuda::PtrStepSzf right)
{
	int size = left.rows * left.cols;
	int globalIdx = getGlobalIdx_2D_2D();

	for (int i = globalIdx; i < size; i += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = i % left.cols;
		int y = i / left.cols;
		if (x <= left.cols - 1 && y <= left.rows - 1 && y >= 0 && x >= 0) {
			left(y, x) = left(y, x) * right(y, x);
		}

	}

}

__device__ int getGlobalIdx_2D_2D()
{
	int blockId = blockIdx.x + blockIdx.y * gridDim.x;
	int threadId = blockId * (blockDim.x * blockDim.y) + (threadIdx.y * blockDim.x) + threadIdx.x;
	return threadId;
}


__global__ void HMMTransition(cv::cuda::PtrStepSzf input, cv::cuda::PtrStepSzf output, 
	                            cv::cuda::PtrStepSzf patch, int patch_size)
{
	// Variables for indexing the GpuMat (Passed as PtrStepSz)

	int imgSize = input.rows * input.cols;
	int globalIdx = getGlobalIdx_2D_2D();
	int Row, Col; // Variables to specify Row and Column access
	int width = input.cols;
	int height = input.rows;
	int patch_half = patch_size / 2;
	float sum = 0;
	

	for (int idx = globalIdx; idx < imgSize; idx += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = idx % input.cols;
		int y = idx / input.cols;
		int patchIdx = 0;
		for (int i = -patch_half; i < patch_half + 1; i++) {
			Row = y + i; // Row in GpuMat to access
			int patch_y = patchIdx / patch_size;
			if (Row < 0) {
				Row += height; // If row < 0, wrap around to bottom of image
			}
			if (Row > height - 1) {
				Row = Row % height; // If row > height, wrap around to Bottom of image
			}

			for (int j = -patch_half; j < patch_half + 1; j++) {
			int patch_x = patchIdx % patch_size;	
				Col = x + j; // Column in GpuMat to access

				if (Col < 0) { // If Col < 0, wrap around to right side of image
					Col += width;
				}
				if (Col > width - 1) { // If Col > width, wrap around to left side of image
					Col = Col % width;
				}
					
				sum += patch(patch_y, patch_x) * input(Row, Col);
				patchIdx++;
			}
		}
		
		output(y, x) = sum;

	}

}