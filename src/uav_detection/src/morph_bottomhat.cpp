#include <ros/ros.h>

#include <image_transport/image_transport.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

#define MORPH_RECT 0
#define MORPH_BOTTOMHAT 6

class BottomHatFilter{

	ros::NodeHandle nh_;
	image_transport::ImageTransport it_;
	image_transport::Subscriber image_sub_;
	image_transport::Publisher image_pub_;

	
public:
	BottomHatFilter() : it_(nh_){

		// Subscribe to a camera/image message. This is what would be published
		// by a ROS node capturing from a camera.
		image_sub_ = it_.subscribe("camera/image",100, &BottomHatFilter::BottomHat, this);
		
		// Publish morphology output.
		image_pub_ = it_.advertise("image_morph/bottom_hat", 100);    					 

	}

	~BottomHatFilter(){
	
	}

	void BottomHat(const sensor_msgs::ImageConstPtr &msg_in){
		cv_bridge::CvImagePtr cv_ptr; // CvImagePtr used to reference cv Mat

		// Structuring element used in Morphology operations
		cv::Mat element;
		
		// CV Mats storing the output from the vertical and horizontal
		// bottom-hat operations
		cv::Mat vertical, horizontal; 
		cv::Mat output;  // Output mat file to be published

		// Copy message received to new CVImagePtr
		try {
		cv_ptr = cv_bridge::toCvCopy(msg_in, sensor_msgs::image_encodings::TYPE_32FC1);
				}
		catch (cv_bridge::Exception& e) {
			ROS_ERROR("cv_bridge exception: %s", e.what());
		}
		
		// Creating a vertical structuring element
		element = getStructuringElement( MORPH_RECT, cv::Size( 5, 1 ), cv::Point( 2, 0) );
		
		// Performing vertical bottom-hat.
		cv::morphologyEx(cv_ptr->image, vertical, MORPH_BOTTOMHAT, element);

		// Create a horizontal structuring element
		element = getStructuringElement( MORPH_RECT, cv::Size( 1, 5 ), cv::Point( 0, 2 ) );

		// Performing horizontal bottom-hat operation
		cv::morphologyEx(cv_ptr->image, horizontal, MORPH_BOTTOMHAT, element);
		
		// Output is created by combining pixel-by-pixel minimum from
		// the vertical and horizontal outputs.
		output = cv::min(vertical,horizontal);

		// Publish the morphology output
		sensor_msgs::ImagePtr  msg_out = cv_bridge::CvImage(std_msgs::Header(), "32FC1", output).toImageMsg();
		image_pub_.publish(msg_out);

	}

};


int main(int argc, char** argv){

	ros::init(argc, argv, "morph_bottomhat");
	
	// Create new BottomHatFilter object which
	// handles the subscription to images, the morphology 
	// operations, and then publishing the output image.
	BottomHatFilter filter;
	ros::spin();
	return 0;

}