#include <ros/ros.h>

#include "opencv2/cudaarithm.hpp"
#include <image_transport/image_transport.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include "std_msgs/Float32.h"

#include <iostream>

static const std::string WINDOW_TITLE = "Viterbi Output";

// Function to return the global thread index.
// Used for accessing elements of the GpuMat
__device__ int getGlobalIdx_2D_2D();

// Function to find max transitional value from alpha_prev for a
// specific branch
__global__ void maxTrans(cv::cuda::PtrStepSzf input,
                         cv::cuda::PtrStepSzf output, int r);

// Function to add two GpuMats. Right is added to left, and right remains unchanged.
__global__ void addGpuMat(cv::cuda::PtrStepSzf left,
                          cv::cuda::PtrStepSzf right);

// Function takes in four GpuMats, each one being a branch of alpha_prev. The output combines the max
// from each (i,j) location across the four, and returns the result in GpuMat output.
__global__ void maxBranch(cv::cuda::PtrStepSzf A, cv::cuda::PtrStepSzf B,
                          cv::cuda::PtrStepSzf C, cv::cuda::PtrStepSzf D,
                          cv::cuda::PtrStepSzf output);



class ViterbiFilter {
private:
	ros::NodeHandle nh;
	image_transport::ImageTransport it;
	image_transport::Subscriber image_sub;
	ros::Publisher viterbi_pub;
	std_msgs::Float32 msg;

	
	bool init = false;
	cv::cuda::GpuMat alpha_prev[4];
	std::vector<cv::cuda::GpuMat> alpha_r;

public:
	ViterbiFilter() : it(nh)
	{
		image_sub = it.subscribe("image_morph/bottom_hat", 100, &ViterbiFilter::Viterbi, this);
		viterbi_pub = nh.advertise<std_msgs::Float32>("temporal/Viterbi", 1000);   // Publish Viterbi output
		std::cout << "ViterbiFilter constructor called..." << std::endl;

	}

	~ViterbiFilter()
	{

	}

	void Viterbi(const sensor_msgs::ImageConstPtr &msg_in)
	{

		cv_bridge::CvImagePtr cv_ptr; // CvImagePtr used to reference cv Mat

		try {
			cv_ptr = cv_bridge::toCvCopy(msg_in, sensor_msgs::image_encodings::TYPE_32FC1);
		} catch (cv_bridge::Exception& e) {
			ROS_ERROR("cv_bridge exception: %s", e.what());
		}

		// If this is the first frame, initialise the alpha_prev vector
		if (!init) {
			std::cout << "Doing an init" << std::endl;
			initAlpha(cv_ptr->image.cols, cv_ptr->image.rows);
			init = true;
		}

		// Perform viterbi processing on frame
		float y_k = processFrame(cv_ptr->image);
			
		// Publish the output data
		msg.data = y_k;
		viterbi_pub.publish(msg);

	}

	// processFrame returns the test statistic y_k;
	float processFrame(cv::Mat &input)
	{
		// Beta "forgetting" factor
		float Beta = 0.75;

		// Variables for calling CUDA Kernel
		// Variables for calling CUDA Kernel
		dim3 threadsPerBlock(8, 8);
		dim3 numBlocks(	(int)(std::ceil(input.cols / (double)threadsPerBlock.x)),
					(int)(std::ceil(input.rows / (double) threadsPerBlock.y)));

		// Stores the scaled input values
		cv::cuda::GpuMat scaledInput;
		scaledInput.upload(input);
		scaledInput.convertTo(scaledInput, scaledInput.type(), (1 - Beta));
		// Temporary dst for maxTrans output
		cv::cuda::GpuMat dst(input.rows, input.cols, input.type());
		for (int r = 0; r < 4; r++) {
			// Finding maxTrans for branch r
			maxTrans <<< numBlocks, threadsPerBlock >> >(alpha_prev[r], dst, r);	
			cudaDeviceSynchronize(); // Wait for device to complete previous kernel call

			// Add results from
			addGpuMat <<< numBlocks, threadsPerBlock >>>(dst, scaledInput);
			cudaDeviceSynchronize();
			alpha_prev[r] = dst.clone();
	 	}

		cv::cuda::GpuMat gpuOut(input.rows, input.cols, input.type());


		maxBranch << < numBlocks, threadsPerBlock >> >(alpha_prev[0], alpha_prev[1], alpha_prev[2], alpha_prev[3], gpuOut);
		cudaDeviceSynchronize();

		// Calculate test statistics y_k from gpuOut
		double y_k;
		cv::cuda::minMaxLoc(gpuOut, NULL, &y_k, NULL, NULL);

		return y_k;
	}

	void initAlpha(int width, int height)
	{


		cv::Mat alpha_init(width, height, CV_32FC1, cv::Scalar::all(0));
		alpha_prev[0].upload(alpha_init);
		for (int r = 1; r < 4; r++) {

			alpha_prev[r] = alpha_prev[0].clone();
		}

	}


};


int main(int argc, char** argv)
{

	ros::init(argc, argv, "viterbi");
	// Add a ViterbiFilter 
	ViterbiFilter viterbiFilter;
	ros::spin();
	return 0;
}

__device__ int getGlobalIdx_2D_2D()
{
	int blockId = blockIdx.x + blockIdx.y * gridDim.x;
	int threadId = blockId * (blockDim.x * blockDim.y) + (threadIdx.y * blockDim.x) + threadIdx.x;
	return threadId;
}

__global__ void maxTrans(cv::cuda::PtrStepSzf input,
                         cv::cuda::PtrStepSzf output, int r)
{

	// Variables for indexing the GpuMat (Passed in as PtrStepSzf)
	float Beta = 0.75;
	int Row, Col;
	int imgSize = input.rows * input.cols;
	int globalIdx = getGlobalIdx_2D_2D();

	int size = 2; // Size of transitional values patch
	float max = 0.0;

	// xFactor, yFactor control the accessing of pixels
	// by using a positive/negative offset to go up/down or left/right
	int xFactor, yFactor;
  
	switch (r) {
	case 0 : // This it the Upper-Right case
		xFactor = 1;
		yFactor = -1;
		break;
	case 1 : // This is the Upper-Left case
		xFactor = -1;
		yFactor = -1;
		break;
	case 2 : // This is the Lower-Left case
		xFactor = -1;
		yFactor = 1;
		break;
	case 3 : // This is the Lower-Right case
		xFactor = 1;
		yFactor = 1;
		break;
	}


	for (int idx = globalIdx; idx < imgSize; idx += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = idx % input.cols;
		int y = idx / input.cols;
		// Iterate over a size*size patch, finding the max value from that patch
		for (int i = 0; i < size; i++) {
			Row = y + (i * yFactor); // Row in GpuMat to access
			for (int j = 0; j < size; j++) {
				Col = x + (j * xFactor); // Column in GpuMat to access
				if ((Col < input.cols) && (Col >= 0) && (Row >= 0) && (Row < input.rows)) {
					float current = input(Row, Col); 
					if (current > max) {
						max = current; // Update max value
					}
				}
			}
		}
		// Final pixel value is the max
		output(y, x) = Beta * max;

	}

}

__global__ void addGpuMat(cv::cuda::PtrStepSzf left,
                          cv::cuda::PtrStepSzf right)
{
	int size = left.rows * left.cols;
	int globalIdx = getGlobalIdx_2D_2D();

	for (int i = globalIdx; i < size; i += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = i % left.cols;
		int y = i / left.cols;
		if (x <= left.cols - 1 && y <= left.rows - 1 && y >= 0 && x >= 0) {
			left(y, x) = left(y, x) + right(y, x);
		}

	}

}

__global__ void maxBranch(cv::cuda::PtrStepSzf A, cv::cuda::PtrStepSzf B,
                          cv::cuda::PtrStepSzf C, cv::cuda::PtrStepSzf D,
                          cv::cuda::PtrStepSzf output)
{

	int size = A.rows * B.cols;
	int globalIdx = getGlobalIdx_2D_2D();
	float max;

	for (int i = globalIdx; i < size; i += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = i % A.cols;
		int y = i / A.cols;
		max = A(y, x);
		if (max < B(y, x)) {
			max = B(y, x);
		}
		if (max < C(y, x)) {
			max = C(y, x);
		}
		if (max < D(y, x)) {
			max = D(y, x);
		}
		output(y, x) = max;
	}

}

