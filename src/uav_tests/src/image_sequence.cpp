#include <ros/ros.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

static const std::string WINDOW_TITLE = "Original Image";

/********* Image Test Sequence *********
/ This ROS Node is designed to publish a sequence of images used for
/ testing morphology and Viterbi filters. Code is designed to run over a specified
/ image set of .ppm files. Would need to be modified to operate on different file types etc.*/

int main(int argc, char** argv) {
	int start, end; // Start and end indexes;
	int pubRate; // Rate at which images are published (FPS)
	if (argc < 3) {
		std::cout << "A file start and end index must be specified using: " << std::endl;
		std::cout << "image_sequence <Start Index> <End Index> <Pub Rate>" << std::endl;
		std::cout << "Additionally, a publishing rate can be specified, otherwise default of 30 FPS is used." << std::endl;
		return 0;
	}

	start = atoi(argv[1]); // Start index
	end = atoi(argv[2]); // Ending index

	if(argc < 4){
		pubRate = 30;
	} else {
		pubRate = atoi(argv[3]);
	}

	ros::init(argc, argv, "image_publisher");
	ros::NodeHandle n;
	image_transport::ImageTransport it(n);
	image_transport::Publisher pub = it.advertise("camera/image", 100);
	
	// Delay to let publisher/subscriber connection initiate
	ros::Rate publisher_sleep(1);
	publisher_sleep.sleep();

	// Counters used to iterate across files
	int total = end - start + 1; // Total number of fiels
	
	int i = 0; // Index to keep track of iterating

	std::cout << "Publishing sequence of images from " << start;
	std::cout << " to " << end << " at " << pubRate << " frames/second." << std::endl;

	ros::Rate loop_rate(pubRate);
	while (n.ok()){
		
		// Once current exceeds total, sequence is complete
		if(i >= total){
			while(n.ok()){
					std::cout << "Sleeping...." << std::endl;
					publisher_sleep.sleep();
					ros::spinOnce();
			}
		}
		
		int image_number = start + i;
		std::stringstream filename;
		filename << image_number << ".ppm";
	//	std::cout << "Processing file: " << filename.str() << std::endl;
		cv::Mat image = cv::imread(filename.str(), CV_LOAD_IMAGE_GRAYSCALE);

		cv::Mat display;
		image.convertTo(image,CV_32FC1); // Convert image to floating point for precision
		image.convertTo(display,CV_32FC1, (1.0/255.0)); // Create a scaled version for display


		sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "32FC1", image).toImageMsg();

		// Add some data to msg header
		msg->header.frame_id = filename.str();
		msg->header.stamp = ros::Time::now();
		
		
		cv::imshow(WINDOW_TITLE, display);
		cv::waitKey(3);
		
		
		pub.publish(msg);
		i++;

		ros::spinOnce();
		loop_rate.sleep();
	}

}
