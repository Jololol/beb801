# BEB801 UFO Detection Ros Nodes

This project is a compilation of ROS nodes used for the detection and tracking of UAV objects.

## uav_tests/image_sequence

This node is used to supply a sequence of images for testing with the tracking nodes. It requires a sequence of images in the *.ppm*  
file format, whose filenames are simply integer values representing the sequence. (e.g. 0.ppm, 1.ppm, 2.ppm etc.).

This node iterates over the files and copies them into an OpenCV *Mat* object. The node then simply publishes the images on the ROS  
topic *camera/image* which can then be subscribed to by any nodes that wish to operate on the images. 


The *image_sequence* node has three command line arguments:

  * Start - An integer value specifying the first file index.
  * End - An integer value specifying the final file index.
  * Publishing Rate (Optional) - An integer value, specifying the publishing rate in Hz.

A typical launch configuration would look like:

    rosrun uav_tests image_sequence 0 1000 60

This would iterate over a sequence of images from 0.ppm through to 1000.ppm publishing at a rate of 60 Hz. 

## uav_detection/morph_bottomhat

This node performs a bottom-hat morphological filtering on a sequence of images. The node subscribes to the topic *camera/image*.  
Once the morphological filtering has been processed, the morphological output is published on the topic *image_morph/bottom_hat*

This node makes use of the OpenCV libraries and will required OpenCV 3 to be installed in order to run.

This node has no command line arguments and is simply launched using the command: 

    rosrun uav_detection morph_bottomhat

## uav_detection/viterbi

This node performs temporal filtering using a Viterbi-like filter on a sequence of images. The node subscribes to the topic *image_morph/bottom_hat*.  

This node makes use of OpenCV 3 and CUDA and will require both of these be installed in order to run.

Once the viterbi filtering has been processed, the test statistic is computed and published on the topic *temporal/viterbi*

This node is simply launched using the command: 

    rosrun uav_detection viterbi

## uav_detection/hmm

This node performs temporal filtering using a HMM filter on a sequence of images. The node subscribes to the topic *image_morph/bottom_hat*.  

This node makes use of OpenCV 3 and CUDA and will require both of these be installed in order to run.

Once the HMM filtering has been processed, the test statistic is computed and published on the topic *temporal/HMM*

The *hmm* node has two command line arguments:

  * Patch Type - An integer value between 1-4 specifying the type of patch:
    - 1: A center patch.
    - 2: A bottom patch.
    - 3: A bottom left patch.
    - 4: A bottom right patch.
  * Patch Size - An integer value specifying the size of the patch. As all patches are a NxN square with a middle location, this number is  
    always rounded up to the nearest odd number.

An example launch configuration of this node is shown as:

    rosrun uav_detection hmm 2 5

This would launch the HMM filter with the patch type being a bottom patch, and the size being 5.
    